# LIS3781 – Advanced Database Management

## Michael Proeber

### LIS3781 Requirements

*Course Work Links:*

- [A1 README.md](a1/README.md "A1 README.md file")
    - Install Git
    - Install MySQL
    - Provide screenshots of installations and ERD
    - Create Bitbucket repos
    - Complete Bitbucket tutorial (BitbucketStationLocations)
    - Provide Bitbucket links
    - Provide Git command descriptions

- [A2 README.md](a2/README.md "A2 README.md file")
    - Chapter questions
    - MySQL SQL code to create company and customer tables with data, indexes, and constraints forward engineered on both CCI and local MySQL
    - Query result sets, including grant statements

- [A3 README.md](a3/README.md "A3 README.md file")
    - Chapter questions
    - Oracle SQL code to create customer, commodity, and order tables with data, indexes and constraints forward engineered on remote Oracle server
    - Data query SQL code and screenshots

- [P1 README.md](p1/README.md "P1 README.md file")
    - Chapter questions
    - MySQL SQL code to create municipal court case database with data and constraints forward engineered to CCI MySQL remote server
    - Entity Relationship Diagram

- [A4 README.md](a4/README.md "A4 README.md file")
    - Chapter questions
    - T-SQL code to create office supply company database storing person and order information forward engineered to remote Microsoft SQL Server
    - Entity Relationship Diagram

- [A5 README.md](a5/README.md "A5 README.md file")
    - Chapter questions
    - T-SQL code to create improved office supply company database with more detailed sales and location data, forward engineered to remote Microsoft SQL Server
    - Entity Relationship Diagram

- [P2 README.md](p2/README.md "P2 README.md file")
    - Chapter questions
    - Install MongoDB
    - Learn basic MongoDB shell commands and functions