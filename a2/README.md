# LIS3781 – Advanced Database Management

## Michael Proeber

### Assignment #2 Requirements

1. Chapter questions
2. SQL code to create company and customer tables with data, indexes, and constraints forward engineered on both CCI and local MySQL
3. Query result sets, including grant statements

#### README.md file should include the following items

1. Screenshots of SQL code
2. Screenshots of populated tables
3. Bitbucket links

#### Assignment Screenshots

##### Screenshots of [SQL code](docs/lis3781_a2_solutions.sql)

![Company SQL code](img/company_sql.png)
![Customer SQL code](img/customer_sql.png)

##### Screenshots of populated company and customer tables

![Company table](img/company_result_set.png)
![Customer table](img/customer_result_set.png)

##### Screenshot of granting privileges

![Granting privileges to user1 and user2](img/1_and_2.png)

##### Screenshots of #3 – Verifying database/table permissions

![Questions 3a and 3c](img/3_a_and_c.png)
![Question 3b](img/3b.png)

##### Screenshot of #4 – Displaying user and version using user2

![Question 4](img/4.png)

##### Screenshot of #5 – List tables as admin

![Question 5](img/5.png)

##### Screenshot of #6 – Display table structures as admin

![Question 6](img/6.png)

##### Screenshot of #7 – Display table data as admin

![Question 7a](img/7a.png)
![Question 7b](img/7b.png)

##### Screenshot of #8 – SQL insert on company and customer as user1

![Question 8](img/8.png)

##### Screenshot of #9 – SQL select and delete as user2

![Question 9](img/9.png)

##### Screenshot of #10 – Remove both tables as admin

![Question 10](img/10.png)

#### Bitbucket Links

*Course repo:*
[Course Repository](https://bitbucket.org/mp20n/lis3781/src/master/ "Course repository")

*Assignment directory:*
[A2 Repository Directory](https://bitbucket.org/mp20n/lis3781/src/master/a2/ "A2 Repository Directory")
