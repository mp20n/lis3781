drop database if exists mp20n;
create database if not exists mp20n;
use mp20n;

drop table if exists company;
create table if not exists company (
    cmp_id int unsigned not null auto_increment primary key,
    cmp_type enum('C-Corp', 'S-Corp', 'Non-Profit-Corp', 'LLC', 'Partnership'),
    cmp_street varchar(45) not null,
    cmp_city varchar(25) not null,
    cmp_state char(2) not null,
    cmp_zip char(9) not null,
    cmp_phone bigint unsigned not null,
    cmp_ytd_sales decimal(10,2) not null,
    cmp_email varchar(100) null,
    cmp_url varchar(45) null,
    cmp_notes varchar(255) null
)
engine = InnoDB character set utf8 collate utf8_general_ci;

show warnings;

insert into company
values
(null, 'C-Corp', '123 Street', 'Tallahassee', 'FL', '323040000', '3524642961', '12345678', 'c.corp@company.com', 'https://www.c-corp.com', null),
(null, 'S-Corp', '321 Road', 'San Francisco', 'CA', '999990000', '3523253734', '87654321', 's.corp@company.com', 'https://www.s-corp.com', null),
(null, 'Non-Profit-Corp', '555 Way', 'New York', 'NY', '123450000', '1112223333', '44443333', 'non.profit.corp@company.com', 'https://www.non-profit-corp.com', null),
(null, 'LLC', '432 Lane', 'Washington', 'DC', '543210000', '4443332222', '22224444', 'llc@company.com', 'https://www.llc.com', null),
(null, 'Partnership', '623 Place', 'Ocala', 'FL', '344760000', '2223334444', '55500000', 'partnership@company.com', 'https://www.partnership.com', null);

show warnings;






drop table if exists customer;
create table if not exists customer (
    cus_id int unsigned auto_increment not null primary key,
    cmp_id int unsigned not null,
    cus_ssn binary(64) not null,
    cus_salt binary(64) not null,
    cus_type enum('Loyal', 'Discount', 'Impulse', 'Need-Based', 'Wandering'),
    cus_first varchar(25) not null,
    cus_last varchar(35) not null,
    cus_street varchar(45),
    cus_city varchar(30),
    cus_state char(2),
    cus_zip char(9),
    cus_phone bigint unsigned not null,
    cus_email varchar(100),
    cus_balance decimal(7,2),
    cus_tot_sales decimal(7,2),
    cus_notes varchar(255),

    unique index ux_cus_ssn (cus_ssn asc),
    index idx_cmp_id (cmp_id asc),

    constraint fk_customer_company
        foreign key (cmp_id)
        references company (cmp_id)
        on delete restrict
        on update cascade
)
engine = InnoDB character set utf8 collate utf8_general_ci;

show warnings;

set @salt=random_bytes(64);

insert into customer
values
(null, 2, unhex(sha2(concat(@salt, 111223333), 512)), @salt, 'Loyal', 'Michael', 'Proeber', '322 Ausley Road Apt C', 'Tallahassee', 'FL', '323040000', '3524642961', 'michaelproeber@gmail.com', '50', '60', null),
(null, 3, unhex(sha2(concat(@salt, 222334444), 512)), @salt, 'Discount', 'Zoe', 'Pallas', '123 Road', 'Tuscaloosa', 'AL', '444440000', '3522925631', 'zoeproeber@gmail.com', null, null, null),
(null, 4, unhex(sha2(concat(@salt, 333445555), 512)), @salt, 'Impulse', 'David', 'Proeber', '10432 SW 42nd Ave', 'Ocala', 'FL', '344760000', '3528754516', 'davidproeber@gmail.com', null, null, null),
(null, 5, unhex(sha2(concat(@salt, 444556666), 512)), @salt, 'Need-Based', 'Andrea', 'Proeber', '10432 SW 42nd Ave', 'Ocala', 'FL', '344760000', '3528171600', 'andreaproeber@gmail.com', null, null, null),
(null, 1, unhex(sha2(concat(@salt, 555667777), 512)), @salt, 'Wandering', 'Xan', 'Fenton', '10432 SW 42nd Ave', 'Ocala', 'FL', '344760000', '3523223444', 'email@gmail.com', null, null, null);

show warnings;
select * from company;
select * from customer;