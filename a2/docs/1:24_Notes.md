* Hashing values obfuscates data; if a hacker gets in, they can't see a customer's SSN or password in plaintext
* Hashing is one-way; a string is hashed into a preset hash value, but the hash value can't be un-hashed, i.e. converted back to the original string
* The hash algorithm makes the string a specific hash and will always return that hash for that particular string
* "Rainbow tables" contain hashes of common passwords that hackers use to access accounts, so to protect against this you have to "salt" the hash, or add a randomized value to the beginning of the string to hash

# Process of hashing for MySQL (won't work for MS SQL Server) using Securing_Data.png for A2
- Follow along this process on local MySQL via commandline, instructions to login to AMPPS MySQL is on bottom of Notes on Canvas

select md5('insertSSN');
- This hashes "insertSSN" using md5. 

select sha2('insertSSN', 512)
- SHA-2 is another hashing algorithm.

set @salt=RANDOM_BYTES(64);
- random_bytes() is a function that creates a random value. This will be used for the salt.
- salt is named "salt" for the assignment purposes; never name the salt attribute "salt" in a professional environment.
- @ symbol is used to define and refer to variables; "@salt" is the variable.
- Create unique salt for each record, but you can use the same salt for multiple attributes with sensitive data.

set @sensitive_data='insertSSN';

select sha2(concat(@salt, @sensitive_data), 512);
- concat(@salt, @sensitive_data) concatenates the salt and data, and sha2 hashes that concatenated string.

select length(sha2(concat(@salt, @sensitive_data), 512));
- This shows the number of bytes that the hash will take up, which should be 128.

- We can halve this by converting the hash into binary, which also further obfuscates the data:
select unhex(sha2(concat(@salt, @sensitive_data), 512));

- If we check the length again, we see that it is now 64 bytes instead of 128!
select length(unhex(sha2(concat(@salt, @sensitive_data), 512)));

## Now test!

drop table if exists user;
create table user (
    uid int unsigned primary key auto_increment,
    ssn binary(64) not null,
    salt binary(64) not null
);

set @ssn='insertSSN'; -- This variable is set to the given value in a stored procedure that will insert the hash of the data.
set @salt=random_bytes(64);

insert into user
(uid, ssn, salt)
values
(null, unhex(sha2(concat(@salt, @sensitive_data), 512)), @salt);

select * from user; -- Binary data will display as gibberish

select uid from user 
where ssn=unhex(sha2(concat(salt, 'ssn'), 512));
-- "salt" refers to the attribute, not the variable, and 'ssn' refers to the value given in a stored procedure.





* Also went over Granting_Privileges.pdf