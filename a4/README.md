# LIS3781 – Advanced Database Management

## Michael Proeber

### Assignment #4 Requirements

1. Chapter questions
2. SQL code to create office supply company database storing person and order information, forward engineered to remote Microsoft SQL Server
3. Entity Relationship Diagram

#### README.md file should include the following items

1. Entity Relationship Diagram
2. SQL code link
3. Bitbucket links

#### Assignment Screenshots

##### Screenshot of Entity Relationship Diagram

[![Entity Relationship Diagram](img/ERD.png)](img/ERD.png)

#### Link to SQL code

[SQL code](docs/lis3781_a4_solutions.sql)

#### Link To Queries

[Query SQL code](docs/sql_queries.sql)

#### Link To Learning MySQL

[Learning MySQL code](docs/Learning_MySQL.sql)

#### Bitbucket Links

*Course repo:*
[Course Repository](https://bitbucket.org/mp20n/lis3781/src/master/ "Course repository")

*Assignment directory:*
[A3 Repository Directory](https://bitbucket.org/mp20n/lis3781/src/master/a3/ "A3 Repository Directory")
