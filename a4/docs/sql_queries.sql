--  1. Create a view that displays the sum of all paid invoice totals for each customer, sort by the largest invoice total sum appearing first.
--In MS SQL SERVER: do *NOT* use ORDER BY clause in *VIEWS* (non-guaranteed behavior)
create view dbo.v_paid_invoice_total as
  select p.per_id, per_fname, per_lname, sum(inv_total) as sum_total, FORMAT(sum(inv_total), 'C', 'en-us') as paid_invoice_total 
  from dbo.person p
    join dbo.customer c on p.per_id=c.per_id 
    join dbo.contact ct on c.per_id=ct.per_cid
    join dbo.[order] o on ct.cnt_id=o.cnt_id
    join dbo.invoice i on o.ord_id=i.ord_id
  where inv_paid !=0
 -- must be contained in group by, if not used in aggregate function
  group by p.per_id, per_fname, per_lname
go


--  2. Create a stored procedure that displays all customers’ outstanding balances (unstored derived attribute based upon the difference of a customer's invoice total and their respective payments). List their invoice totals, what was paid, and the difference.
--In MS SQL SERVER: *only* must use TOP, OFFSET, or FETCH clauses in *VIEWS* with ORDER BY clause
CREATE PROC dbo.sp_all_customers_outstanding_balances AS 
BEGIN
  select p.per_id, per_fname, per_lname, 
  sum(pay_amt) as total_paid, (inv_total - sum(pay_amt)) invoice_diff
    from person p
      join dbo.customer c on p.per_id=c.per_id 
      join dbo.contact ct on c.per_id=ct.per_cid
      join dbo.[order] o on ct.cnt_id=o.cnt_id
      join dbo.invoice i on o.ord_id=i.ord_id
      join dbo.payment pt on i.inv_id=pt.inv_id
     -- must be contained in group by, if not used in aggregate function
  group by p.per_id, per_fname, per_lname, inv_total
  order by invoice_diff desc;
END
GO
-- call stored procedure
exec dbo.sp_all_customers_outstanding_balances;


--  3. Create a stored procedure that populates the sales rep history table w/sales reps’ data when called.
CREATE PROC dbo.sp_populate_srp_hist_table AS 
  INSERT INTO dbo.srp_hist 
  (per_id, sht_type, sht_modified, sht_modifier, sht_date, sht_yr_sales_goal, sht_yr_total_sales, sht_yr_total_comm, sht_notes) 
  -- mix dynamically generated data, with original sales reps' data
  SELECT per_id, 'i', getDate(), SYSTEM_USER, getDate(), srp_yr_sales_goal, srp_ytd_sales, srp_ytd_comm, srp_notes
  FROM dbo.slsrep;
GO
-- call stored procedure
exec dbo.sp_populate_srp_hist_table;


--  4. Create a trigger that automatically adds a record to the sales reps’ history table for every record added to the sales rep table.
if OBJECT_ID(N'dbo.trg_sales_history_insert', N'TR') is not null
drop trigger dbo.trg_sales_history_insert;
go

CREATE TRIGGER dbo.trg_sales_history_insert 
ON dbo.slsrep 
AFTER INSERT AS
BEGIN
  -- declare 
  DECLARE 
  @per_id_v smallint, 
  @sht_type_v char(1), 
  @sht_modified_v date, 
  @sht_modifier_v varchar(45), 
  @sht_date_v date, 
  @sht_yr_sales_goal_v decimal(8,2), 
  @sht_yr_total_sales_v decimal(8,2), 
  @sht_yr_total_comm_v decimal(7,2), 
  @sht_notes_v varchar(255);
  SELECT 
  @per_id_v = per_id, 
  @sht_type_v = 'i',
  @sht_modified_v = getDate(),
  @sht_modifier_v = SYSTEM_USER,
  @sht_date_v = getDate(),
  @sht_yr_sales_goal_v = srp_yr_sales_goal, 
  @sht_yr_total_sales_v = srp_ytd_sales, 
  @sht_yr_total_comm_v = srp_ytd_comm, 
  @sht_notes_v = srp_notes
  FROM INSERTED;
  INSERT INTO dbo.srp_hist 
  (per_id, sht_type, sht_modified, sht_modifier, sht_date, sht_yr_sales_goal, sht_yr_total_sales, sht_yr_total_comm, sht_notes) 
  VALUES
  (@per_id_v, @sht_type_v, @sht_modified_v, @sht_modifier_v, @sht_date_v, @sht_yr_sales_goal_v, @sht_yr_total_sales_v, @sht_yr_total_comm_v,@sht_notes_v);
END
GO


--  5. Create a trigger that automatically adds a record to the product history table for every record added to the product table.
CREATE TRIGGER dbo.trg_product_history_insert 
ON dbo.product 
AFTER INSERT AS
BEGIN
    DECLARE 
      @pro_id_v smallint, 
      @pht_modified_v date, -- when recorded
      @pht_cost_v decimal(7,2), 
      @pht_price_v decimal(7,2), 
      @pht_discount_v decimal(3,0), 
      @pht_notes_v varchar(255);
      SELECT 
      @pro_id_v = pro_id, 
      @pht_modified_v = getDate(),
      @pht_cost_v = pro_cost, 
      @pht_price_v = pro_price, 
      @pht_discount_v = pro_discount, 
      @pht_notes_v = pro_notes
      FROM INSERTED;
      INSERT INTO dbo.product_hist 
      (pro_id, pht_date, pht_cost, pht_price, pht_discount, pht_notes) 
      VALUES
      (@pro_id_v, @pht_modified_v, @pht_cost_v, @pht_price_v, @pht_discount_v,@pht_notes_v);
END
GO


--  EC - Create a stored procedure that updates sales reps’ yearly_sales_goal in the slsrep table, based upon 8% more than their previous year’s total sales (sht_yr_total_sales), name it sp_annual_salesrep_sales_goal. (See Notes above.)
CREATE PROC dbo.sp_annual_salesrep_sales_goal AS 
BEGIN
  -- update is based solely upon 8% of each sales rep's previous year's individual total sales
  UPDATE slsrep
  SET srp_yr_sales_goal = sht_yr_total_sales * 1.08
  from slsrep as sr
    JOIN srp_hist as sh
    ON sr.per_id = sh.per_id
  where sh.sht_date='2014-01-01';
END
GO
-- call stored procedure
exec dbo.sp_annual_salesrep_sales_goal;