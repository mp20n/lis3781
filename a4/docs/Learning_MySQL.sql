-- Learning MySQL – LIS3781 
 
-- 1. Log in remotely using command-line--MySQL Workbench is *not* permitted. Use the lis3781 database.
 
-- 2. Display user, timestamp, and MySQL version: 
select user(), now(), version();

-- 3. Display *your* grants:
show grants;
 
-- 4. Display all databases. 
show databases; 

-- 5. Display all tables for lis3781:
show tables;

-- 6. Display structure for each table: 
describe each_table;

-- 7. Display data for each table:
select * from each_table;

-- 8. Display create statement for *one* table:
show create table president;
 
--Required reports: 
 
-- 1. Display all president's first and last names, and birth dates, for those presidents born before Jan. 1, 1750: 
select first_name, last_name, birth
from president
where birth < '1750-01-01';
 
-- 2. Display all president's first and last names, birth dates and states, for those presidents born in either Virginia or Massachusetts:
select first_name, last_name, birth, state
from president
where state in ('VA', 'MA');
 
-- 3. Display all president's first and last names, birth dates and states, for those presidents born before Jan. 1, 1750, and, born in either Virginia or Massachusetts:
select first_name, last_name, birth, state
from president
where birth < '1750-01-01' and state in ('VA', 'MA');
 
-- 4. Display all president's first and last names, and name suffixes, only for presidents with a name suffix:
select first_name, last_name, suffix
from president
where suffix is not null;
 
-- 5. Display all president's first and last names, and births, only for the five most recently born presidents:
select first_name, last_name, birth
from president
order by birth desc
limit 5;
 
-- 6. Display all president's first and last names, and births, only for the second set of five most recently born presidents (i.e., 6 - 10):
select first_name, last_name, birth
from president
order by birth desc
limit 5,5;
 
-- 7. Display a random set of three president's first and last names, and births:
select first_name, last_name, birth
from president
order by rand()
limit 3;
 
-- 8. Display all president's first and last names as "Presidents' Names," birth cities and 
-- states as "Places of Birth," and birth and death dates, for those presidents who died 
-- between 1970 and 2010, order by most recent deaths:
select concat(first_name, " ", last_name) as "Presidents' Names", concat(city, ", ", state) as "Places of Birth", birth, death
from president
where death between '1970-01-01' and '2010-01-01'
order by death desc;
-- or
select concat(first_name, " ", last_name) as "Presidents' Names", concat(city, ", ", state) as "Places of Birth", birth, death
from president
where death >= '1970-01-01' and death < date_add('1970-01-01', interval 40 year)
order by death desc;
 
-- 9. display all president's first and last names, and birth dates, for those presidents born in the month of March, sort in descending order of birth date 
select concat(first_name, " ", last_name) as "Presidents' Names", birth
from president
where birth like "%-03-%"
order by birth desc;

-- 10. display all president's first and last names, and death dates, for those presidents who died the day after Christmas, sort in descending order of death date 
select first_name, last_name, death
from president
where death like "____-12-26"
order by death desc;
 
-- 11. display all member's first and last names, and expiration dates, whose expiration dates are at least six months ago. NOTE: After having found this query result set, the members could be sent a reminder, or the records could be purged. 
select last_name, first_name, expiration, date_sub(curdate(), interval 6 month) as 'six months ago'
from member
where expiration < date_sub(curdate(), interval 6 month)
order by expiration desc;

-- 12. display top 10 oldest presidents who are now deceased, in descending order of age 
select last_name, first_name, timestampdiff(year, birth, death) as 'age', birth, death
from president
where death is not null
order by age desc
limit 10;
 
-- 13. display all president's first and last names, birth and death dates, and ages, whose names begin with "w," order by last name:  
select last_name, first_name, timestampdiff(year, birth, death) as 'age'
from president
where last_name like 'w%'
order by last_name;


-- 14. display all president's first and last names, birth and death dates (death dates may be NULL), whose names have only four characters, order by last name:  
select first_name, last_name, birth, death
from president
where last_name like '____'
order by last_name;

-- 15. display a list of all the states in which presidents have been born, in ascending order, remove duplicate values 
select distinct state
from president
order by state asc;

-- 16. display only the number of members there are
select count(member_id)
from member;
 
-- 17. display how many quizzes have been given to the class so far
select count(category)
from grade_event
where category = 'Q';

-- 18. display the number of members, number of email addresses, and number of members w/o lifetime memberships 
-- (NULL values in expiration indicate lifetime memberships) 
select count(*), count(email), count(expiration)
from member;

-- 19. display the number of different states in which presidents have been born, remove duplicate values (similar to #15) 
select count(distinct state)
from president;

-- 20. display the total number of students 
select count(*)
from student;
 
-- 21. display how many men and women there are in one query 
select (select count(*) from student where sex = 'm') as 'men', (select count(*) from student where sex = 'f') as 'women';
-- OR
select sex, count(*)
from student
group by sex;

-- 22. display the TOP FOUR states having the largest number of presidents born in those states, sorted by the greatest number of presidents born in each of those states. 
select state, count(*)
from president
group by state
order by count(*) desc;

-- 23. display how many presidents were born in each month of the year, sorted by month 
select month(birth), monthname(birth), count(*)
from president
group by month(birth), monthname(birth)
order by month(birth) asc;

-- 24. display the states, and number of presidents born in each state, with two or more  presidents born there, sorted by the greatest number of presidents in each of those states. 
-- NOTE: In addition, this query finds duplicate values, to find nonduplicated values, use having count = 1. 
select state, count(*)
from president
group by state
having count(*) > 1
order by count(*) desc;

-- display the states where only one president was born, sorted by state 
select state
from president
group by state
having count(*) = 1
order by state;

-- 25. Display aggregate summaries: 
-- display the minimum, maximum, span (difference between maximum and minimum scores), total, average, and number of scores for each quiz and test taken to date
select min(score), max(score), max(score) - min(score) as span, sum(score), avg(score), count(score)
from score
group by event_id;