select '4) Create a trigger that automatically adds a record to the judge history table for every record **added** to the judge table.';
do sleep(5); 
-- Note: technically, a judge could already be in the person table, yet not added to the judge table. Or, here, add a new person. 
-- add 16th person (a judge), before adding person to judge table (salt and hash per_ssn, and store unique salt, per_salt) 

select 'show person data *before* adding person record' as ''; 
select per_id, per_fname, per_lname from person;
do sleep(5); 

-- give person a unique randomized salt, then hash and salt SSN 
SET @salt=RANDOM_BYTES(64); -- salt includes unique random bytes for each user 
SET @num=000000000; --Note: already provided random SSN from111111111 - 999999999, inclusive above
SET @ssn=unhex(sha2(concat(@salt, @num), 512)); -- salt and hash person's SSN 000000000  

INSERT INTO person  
(per_id, per_ssn, per_salt, per_fname, per_lname, per_street, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes) 
values 
(NULL, @ssn, @salt, 'Bobby', 'Sue', '123 Main St', 'Panama City Beach', 'FL', 324530221, 'bsue@fl.gov', '1962-05-16', 'j', 'new district judge'); 

select 'show person data *after* adding person record' as ''; 
select per_id, per_fname, per_lname from person; 
do sleep(5);

select 'show judge/judge_hist data *before* AFTER INSERT trigger fires (trgJudge_history_after_insert)' as '';
select * from judge;
select * from judge_hist;
do sleep(7);

-- Note: use user() rather than current_user()
-- current_user() indicates definer of stored routine/view/trigger/event, not client user

-- after insert of judge table, insert into judge_hist table
drop trigger if exists trg_judge_history_after_insert;

delimiter //
create trigger trg_judge_history_after_insert
after insert on judge
for each row
begin
    insert into judge_hist
    (per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
    values
    (NEW.per_id, NEW.crt_id, current_timestamp(), 'i', NEW.jud_salary, concat('modifying user: ', user(), ' | notes: ', NEW.jud_notes));
end //
delimiter ;

select 'fire trigger by inserting record into judge table' as '';
do sleep(5);

insert into judge
(per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
values
((select count(per_id) from person), 3, 175000, 3, 'transferred from neighboring jurisdiction');

select 'show judge/judge_hist data *after* AFTER INSERT trigger fires (trg_judge_history_after_insert' as '';
select * from judge;
select * from judge_hist;
do sleep(7);