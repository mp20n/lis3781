-- Data for table person 

START TRANSACTION; 

INSERT INTO person 
(per_id, per_ssn, per _ salt, per_fname, per_lname, per_street, per _ city, per _ state, per _ zip, per_email, per_dob, per_type, per _ notes) 
VALUES 
(NULL, NULL, NULL, 'Steve', 'Rogers', '437 southern Drive', 'Rochester', 'NY', 324402222, 'srogers@comcast.net', '1923-10-03', NULL), 
(NULL, NULL, NULL, 'Bruce', 'Wayne','1007 Mountain Drive', 'Gotham', 'NY', 003208440, 'bwayne@knology.net', '1968-03-20', 'c', NULL), 
(NULL, NULL, NULL, 'Peter', 'Parker', '20 Ingram Street', 'New York', 'NY', 102862341, 'pparker@msn.com', '1988-09-12', 'c', NULL), 
(NULL, NULL, NULL, 'Jane', 'Thompson', '13563 Ocean View Drive', 'Seattle', 'WA', 032084409, 'jthompson@gmail.com', '1978-05-08', 'c', NULL), 
(NULL, NULL, NULL, 'Debra', 'Steele', '543 Oak Ln', 'Milwaukee', 'WI', 286234178, 'dsteele@verizon.net', '1994-07-19', 'c', NULL), 
(NULL, NULL, NULL, 'Tony', 'Stark', '332 Palm Avenue', 'Malibu', 'CA', 902638332, 'tstark@yahoo.com', '1972-05-04', NULL), 
(NULL, NULL, NULL, 'Hank', 'Pymi', '2355 Brown Street', 'Cleveland', 'OH', 022348890, 'hpym@aol.com', '1980-08-28', NULL), 
(NULL, NULL, NULL, 'Bob', 'Best', '4902 Avendale Avenue', 'Scottsdale', 'AZ', 872638332, 'bbest@yahoo.com', '1992-02-10', 'a', NULL), 
(NULL, NULL, NULL, 'Sandra', 'Dole', '87912 Lawrence Ave', 'Atlanta', 'GA', 002348890, 'sdole@gmail.com', '1990-01-26', 'a', NULL), 
(NULL, NULL, NULL, 'Ben', 'Avery', '6432 Thunderbird Ln', 'Sioux Falls', 'SD', 562638332, 'bavery@hotmail.com', '1983-12-24', 'a', NULL), 
(NULL, NULL, NULL, 'Arthur', 'Curry', '3304 Euclid Avenue', 'Miami', 'FL', 000219932, 'acurry@gmail.com', '1975-12-15', NULL), 
(NULL, NULL, NULL, 'Diana', 'Price', '944 Green Street', 'Las Vegas', 'NV', 332048823, 'dprice@symaptico.com', '1980-08-22', 'j', NULL), 
(NULL, NULL, NULL, 'Adam', 'JurriS', '98435 Valencia Dr.', 'Gulf Shores', 'AL', 870219932, 'ajurris@gmx.com', '1995-01-31', 'j', NULL), 
(NULL, NULL, NULL, 'Judy', 'Sleen', '56343 Rover Ct.', 'Billings', 'MT', 672048823, 'jsleen@symaptico.com', '1970-03-22', 'j', NULL), 
(NULL, NULL, NULL, 'Bill', 'Neiderheim', '43567 Netherland Blvd', 'South Bend', 'IN', 320219932, 'bneiderheim@comcast.net', '1982-03-13', 'j', NULL); 

COMMIT; 






