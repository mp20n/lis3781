-- Populate person table with hashed and salted SSN numbers. *MUST* include salted value in DB! 
DROP PROCEDURE IF EXISTS CreatePersonSSN; 
DELIMITER $$ 
CREATE PROCEDURE CreatePersonSSN() 
BEGIN 
-- MySQL Variables: https://stackoverfIow.com/questions/11754781/how-to-decIare-a-variabIe-in-mysqI 
    DECLARE x, y INT; 
    SET x = 1; 

    -- dynamically set loop ending value (total number of persons) 
    select count(*) into y from person; 
    -- select y; display number of persons (only for testing) 

    WHILE x <= y DO
        -- give each person a unique randomized salt, and hashed and salted randomized SSN. 
        -- Note: this demo is *only* for showing how to include salted and hashed randomized values for testing purposes! 
        SET @salt=RANDOM_BYTES(64); -- salt includes unique random bytes for each user when looping 
        SET @ran_num=FLOOR(RAND()*(999999999-111111111+1))+111111111; -- random 9-digit SSN from111111111 - 999999999, inclusive 
        SET @ssn=unhex(sha2(concat(@salt, @ran_num), 512)); -- each user's SSN value is uniquely randomized and uniquely salted! 

        -- RAND([N]): Returns random floating-point value v in the range O < = v < 1.0 
        -- Documentation: https://dev.mysql.com/doc/refman/5.7/en/mathematical-functions.html#function_rand 

        update person 
        set per_ssn=@ssn, per_salt=@salt 
        where per_id=x;

        SET x = x + 1;
    END WHILE; 
END$$

DELIMITER ; 
call CreatePersonSSN();