drop schema if exists mp20n;

USE mp20n; 

-- Table person 
-- NOTE: allowi per_ssn and per_salt to be null, in order to use stored proc CreatePersonSSN below 
DROP TABLE IF EXISTS person; 
CREATE TABLE IF NOT EXISTS person (
    per_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT primary key, 
    per_ssn BINARY(64) NULL, 
    per_salt binary(64) NULL COMMENT '*only* demo purposes - do *NOT* use *salt* in the name!', 
    per_fname VARCHAR(25) NOT NULL, 
    per_lname VARCHAR(35) NOT NULL, 
    per_street VARCHAR(30) NOT NULL, 
    per_city VARCHAR(40) NOT NULL, 
    per_state CHAR(2) NOT NULL, 
    per_zip CHAR(9) UNSIGNED NOT NULL, 
    per_email VARCHAR(100) NOT NULL, 
    per_dob DATE NOT NULL, 
    per_type ENUM('a','c','j') NOT NULL, 
    per_notes VARCHAR(255) NULL,
    UNIQUE INDEX ux_per_ssn (per_ssn ASC) 
)

ENGINE = InnoDB 
DEFAULT CHARACTER SET=utf8mb4
COLLATE = utf8mb4_0900_ai_ci; 

SHOW WARNINGS;

-- Table attorney
