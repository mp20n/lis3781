-- 1. Create a view that displays attorneys’ *full* names, *full* addresses, ages, hourly rates, the bar names that they’ve passed, as well as their specialties, sort by attorneys’ last names.


-- 2. Create a stored procedure that displays how many judges were born in each month of the year, sorted by month.


-- 3. Create a stored procedure that displays *all* case types and descriptions, as well as judges’ *full* names, *full* addresses, phone numbers, years in practice, for cases that they presided over, with their start and end dates, sort by judges’ last names.


-- 4. Create a trigger that automatically adds a record to the judge history table for every record added to the judge table.


-- 5. Create a trigger that automatically adds a record to the judge history table for every record modified in the judge table.


-- 6. Create a one-time event that executes one hour following its creation, the event should add a judge record (one more than the required five records), have the event call a stored procedure that adds the record (name it one_time_add_judge).


-- Extra Credit — Create a scheduled event that will run every two months, beginning in three weeks, and runs for the next four years, starting from the creation date. The event should not allow more than the first 100 judge histories to be stored, thereby removing all others (name it remove_judge_history).
