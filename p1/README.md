# LIS3781 – Advanced Database Management

## Michael Proeber

### Project #1 Requirements

1. Chapter questions
2. MySQL SQL code to create municipal court case database with data and constraints forward engineered to CCI MySQL remote server
3. Entity Relationship Diagram

#### README.md file should include the following items

1. SQL code
2. Entity Relationship Diagram
3. Bitbucket links

#### Project Screenshots

##### Screenshots of person table insert transaction and select statements

[![Person inserts](img/person_inserts.png "Populated person table")](img/person_inserts.png)
[![Person select](img/person_select.png "Populated person table select statement")](img/person_select.png)

##### Screenshot of Entity Relationship Diagram

[![ERD](img/ERD.png)](img/ERD.png)

#### Link To SQL code

[SQL create and insert statements](docs/lis3781_p1_solutions.sql "MySQL SQL code file")

#### Link To Queries

[Query SQL code and screenshots](docs/sql_queries "MySQL SQL queries directory")

#### Link To Entity Relationship Diagram

[Entity Relationship Diagram](docs/P1_ERD.mwb "ERD file")

#### Bitbucket Links

*Course repo:*
[Course Repository](https://bitbucket.org/mp20n/lis3781/src/master/ "Course repository")

*Project directory:*
[P1 Repository Directory](https://bitbucket.org/mp20n/lis3781/src/master/p1/ "P1 Repository Directory")
