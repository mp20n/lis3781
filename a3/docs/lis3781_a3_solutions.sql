drop sequence seq_cus_id; -- create customer id increment sequence
create sequence seq_cus_id
    start with 1
    increment by 1
    minvalue 1
    maxvalue 10000;

drop table customer cascade constraints purge;
create table customer (
    cus_id number(3) not null primary key,
    cus_fname varchar2(15) not null,
    cus_lname varchar2(30) not null,
    cus_street varchar2(35) not null,
    cus_city varchar2(35) not null,
    cus_state char(2) not null,
    cus_zip number(9) not null,
    cus_phone number(10) not null,
    cus_email varchar2(100),
    cus_balance number(7,2),
    cus_notes varchar2(255)
);


drop sequence seq_com_id; -- create commodity id increment sequence
create sequence seq_com_id
    start with 1
    increment by 1
    minvalue 1
    maxvalue 10000;

drop table commodity cascade constraints purge;
create table commodity (
    com_id number not null primary key,
    com_name varchar2(25),
    com_price number(8,2) not null,
    com_notes varchar2(255)
    -- constraint uq_com_name unique(com_name)
);
create unique index uq_com_name on commodity(lower(com_name)); -- enforce case-insensitive unique commodity names


drop sequence seq_ord_id; -- create order id increment sequence
create sequence seq_ord_id
    start with 1
    increment by 1
    minvalue 1
    maxvalue 10000;

drop table "order" cascade constraints purge;
create table "order" (
    ord_id number(4) not null primary key, -- max value 9999
    cus_id number,
    com_id number,
    ord_num_units number(5) not null, -- max value 99999
    ord_total_cost number(8,2) not null,
    ord_notes varchar2(255),
    constraint fk_order_customer
        foreign key (cus_id) references customer(cus_id),
    constraint fk_order_commodity
        foreign key (com_id) references commodity(com_id),
    constraint check_unit 
        check(ord_num_units between 1 and 99999),
    constraint check_total 
        check(ord_total_cost between 1 and 999999.99)
);

insert into customer values (seq_cus_id.nextval, 'Michael', 'Proeber', '123 Road', 'Tallahassee', 'FL', 12345, 3524642961, 'michaelproeber@gmail.com', 10000.00, null);
insert into customer values (seq_cus_id.nextval, 'Mark', 'Jowett', '321 Street', 'Tallahassee', 'FL', 54321, 1112223333, 'mjowett@fsu.edu', 5000.00, null);
insert into customer values (seq_cus_id.nextval, 'Liam', 'Perna', '543 Way', 'San Francisco', 'CA', 33333, 2223334444, 'liamperna@gmail.com', 1000.00, null);
insert into customer values (seq_cus_id.nextval, 'Carey', 'Carpenter', '6543 Circle', 'New York', 'NY', 22222, 3334445555, 'careycarpenter@gmail.com', 3000.00, null);
insert into customer values (seq_cus_id.nextval, 'Stefan', 'Babin', '723 Drive', 'Seattle', 'WA', 12346, 4445556666, 'stefanbabin@gmail.com', 6000.00, null);
commit;

insert into commodity values (seq_com_id.nextval, 'DVD Player', 109, null);
insert into commodity values (seq_com_id.nextval, 'Cereal', 4, null);
insert into commodity values (seq_com_id.nextval, 'Scrabble', 30, null);
insert into commodity values (seq_com_id.nextval, 'Licorice', 2, null);
insert into commodity values (seq_com_id.nextval, 'Tums', 3, null);
commit;

insert into "order" values (seq_ord_id.nextval, 1, 2, 5, 20, null);
insert into "order" values (seq_ord_id.nextval, 1, 3, 6, 180, null);
insert into "order" values (seq_ord_id.nextval, 2, 2, 89, 356, null);
insert into "order" values (seq_ord_id.nextval, 3, 1, 5, 545, null);
insert into "order" values (seq_ord_id.nextval, 4, 5, 3, 210, null);
insert into "order" values (seq_ord_id.nextval, 4, 4, 30, 60, null);
insert into "order" values (seq_ord_id.nextval, 5, 4, 100, 200, null);
insert into "order" values (seq_ord_id.nextval, 4, 2, 10, 40, null);
insert into "order" values (seq_ord_id.nextval, 3, 1, 1, 109, null);
insert into "order" values (seq_ord_id.nextval, 4, 1, 2, 218, null);
commit;

select * from customer;
select * from commodity;
select * from "order";