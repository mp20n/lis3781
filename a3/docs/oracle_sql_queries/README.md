# LIS3781 – Advanced Database Management

## Michael Proeber

#### Link to SQL code

[Oracle SQL code](docs/oracle_sql_queries.sql "SQL code")

#### Screenshots

##### 1. Display Oracle version (one method)

![Number 1](img/1.png)

##### 2. Display Oracle version (another method)

![Number 2](img/2.png)

##### 3. Display current user

![Number 3](img/3.png)

##### 4. Display current day/time (formatted, and displaying AM/PM)

![Number 4](img/4.png)

##### 5. Display your privileges

![Number 5](img/5.png)

##### 6. Display all user tables

![Number 6](img/6.png)

##### 7. Display structure for each table

![Number 7](img/7.png)

##### 8. List the customer number, last name, first name, and e-mail of every customer

![Number 8](img/8.png)

##### 9. Same query as above, include street, city, state, and sort by state in descending order, and last name in ascending order

![Number 9](img/9.png)

##### 10. What is the full name of customer number 3? Display last name first

![Number 10](img/10.png)

##### 11. Find the customer number, last name, first name, and current balance for every customer whose balance exceeds $1,000, sorted by largest to smallest balances

![Number 11](img/11.png)

##### 12. List the name of every commodity, and its price (formatted to two decimal places, displaying $ sign), sorted by smallest to largest price

![Number 12](img/12.png)

##### 13. Display all customers’ first and last names, streets, cities, states, and zip codes as follows (ordered by zip code descending)

![Number 13](img/13.png)

##### 14. List all orders not including cereal--use subquery to find commodity id for cereal

![Number 14](img/14.png)

##### 15. List the customer number, last name, first name, and balance for every customer whose balance is between $500 and $1,000, (format currency to two decimal places, displaying $ sign)

![Number 15](img/15.png)

##### 16. List the customer number, last name, first name, and balance for every customer whose balance is greater than the average balance (format currency to two decimal places, displaying $ sign).

![Number 16](img/16.png)

##### 17. List the customer number, name, and *total* order amount for each customer sorted in descending *total* order amount, (format currency to two decimal places, displaying $ sign), and include an alias “total orders” for the derived attribute

![Number 17](img/17.png)

##### 18. List the customer number, last name, first name, and complete address of every customer who lives on a street with "Street" anywhere in the street name

![Number 18](img/18.png)

##### 19. List the customer number, name, and *total* order amount for each customer whose *total* order amount is greater than $1500, for each customer sorted in descending *total* order amount, (format currency to two decimal places, displaying $ sign), and include an alias “total orders” for the derived attribute

![Number 19](img/19.png)

##### 20. List the customer number, name, and number of units ordered for orders with 30, 40, or 50 units ordered

![Number 20](img/20.png)

##### 21. Using EXISTS operator: List customer number, name, number of orders, minimum, maximum, and sum of their order total cost, only if there are 5 or more customers in the customer table, (format currency to two decimal places, displaying $ sign)

![Number 21](img/21.png)

##### 22. Find aggregate values for customers: (Note, difference between count(*) and count(cus_balance), one customer does not have a balance.)

![Number 22](img/22.png)

##### 23. Find the number of unique customers who have orders

![Number 23](img/23.png)

##### 24. List the customer number, name, commodity name, order number, and order amount for each customer order, sorted in descending order amount, (format currency to two decimal places, displaying $ sign), and include an alias “order amount” for the derived attribute

![Number 24](img/24.png)

##### 25. Modify prices for DVD players to $99

![Number 25](img/25.png)
