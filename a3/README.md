# LIS3781 – Advanced Database Management

## Michael Proeber

### Assignment #3 Requirements

1. Chapter questions
2. SQL code to create customer, commodity, and order tables with data and constraints forward engineered to Oracle remote server

#### README.md file should include the following items

1. Screenshots of SQL code
2. Screenshots of populated tables
3. Query code and screenshots
4. Bitbucket links

#### Assignment Screenshots

##### Screenshots of [SQL code](docs/lis3781_a3_solutions.sql)

![Customer SQL code](img/customer_create.png)
![Commodity SQL code](img/commodity_create.png)
![Order SQL code](img/order_create.png)
![Inserts](img/inserts.png)

##### Screenshots of populated customer, commodity, and order tables

![Customer table](img/customer_data.png "Populated customer table")
![Commodity table](img/commodity_data.png "Populated commodity table")
![Order table](img/order_data.png "Populated order table")

#### Link To Queries

[Query SQL code and screenshots](docs/oracle_sql_queries "Oracle SQL queries README")

#### Bitbucket Links

*Course repo:*
[Course Repository](https://bitbucket.org/mp20n/lis3781/src/master/ "Course repository")

*Assignment directory:*
[A3 Repository Directory](https://bitbucket.org/mp20n/lis3781/src/master/a3/ "A3 Repository Directory")
