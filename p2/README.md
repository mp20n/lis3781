# LIS3781 – Advanced Database Management

## Michael Proeber

### Project #2 Requirements

1. Chapter questions
2. Install MongoDB
3. Learn basic MongoDB shell commands and functions

#### README.md file should include the following items

1. NoSQL query code and screenshots
2. Screenshot of MongoDB shell commands
3. Bitbucket links

#### Project Screenshots

##### MongoDB shell commands

[![Shell commands](img/commands.png "Shell commands")](img/commands.png)

#### Link To NoSQL query code

[NoSQL code](docs/MongoDB_Queries.sql "NoSQL MongoDB code file")

#### Link To Query Screenshots

[Query SQL screenshots](docs/queries "NoSQL query directory")

#### Bitbucket Links

*Course repo:*
[Course Repository](https://bitbucket.org/mp20n/lis3781/src/master/ "Course repository")

*Project directory:*
[P2 Repository Directory](https://bitbucket.org/mp20n/lis3781/src/master/p2/ "P2 Repository Directory")
