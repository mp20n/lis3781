-- 1. Display all documents in collection.
db.restaurants.find().pretty()


-- 2. Display the number of documents in collection.
db.restaurants.find().count()

-- Retrieve last document.
db.restaurants.find().sort({_id: -1}).limit(1).pretty()


-- 3. Retrieve 1st 5 documents.
db.restaurants.find().limit(5).pretty()


-- 4. Retrieve restaurants in the Brooklyn borough.
db.restaurants.find( {"borough": "Brooklyn"} ).pretty()

-- Count number of restaurants in the Brooklyn borough.
db.restaurants.find( {"borough": "Brooklyn"} ).count()

-- Case insensitive search using regex
db.restaurants.find( {"borough": /^brooklyn$/i } ).pretty()


-- 5. Retrieve restaurants whose cuisine is American.
db.restaurants.find( {"cuisine": "American"} ).pretty() -- doesn't work because data is incorrect; "American "
db.restaurants.find( {"cuisine": /^american\s$/i} ).pretty()

-- Retrieve restaurants whose cuisine is "like" "Am%" similar to SQL LIKE query
db.restaurants.find( {"cuisine": /^Am/} ) -- like "Am%"
db.restaurants.find( {"cuisine": /Am/} ) -- like "%Am%"
db.restaurants.find( {"cuisine": /an$/} ) -- like "%am"


-- 6. Retrieve restaurants whose borough is Manhattan and cuisine is hamburgers.
db.restaurants.find( {"borough": "Manhattan", "cuisine": "Hamburgers"} ).pretty()

-- Display only name and restaurant ID
db.restaurants.find( {"borough": "Manhattan", "cuisine": "Hamburgers"}, {"_id": 0, "name": 1, "restaurant_id": 1} ).pretty()

-- Sort by name ascending, restaurant_id descending
db.restaurants.find( {"borough": "Manhattan", "cuisine": "Hamburgers"}, {"_id": 0, "name": 1, "restaurant_id": 1} ).sort( {"name": 1, "restaurant_id": -1} ).pretty()


-- 7. Display the number of restaurants whose borough is Manhattan and cuisine is hamburgers.
db.restaurants.find( {"borough": "Manhattan", "cuisine": "Hamburgers"} ).count()


-- 8. Query zip code field in embedded address document. Retrieve restaurants in the 10075 zip code area.
db.restaurants.find( {"address.zipcode": "10075"} ).pretty()
db.restaurants.find( {"address.zipcode": "10075"}, {"_id": 0, "address.zipcode": 1} ).pretty()


-- 9. Retrieve restaurants whose cuisine is chicken and zipcode is 10024.
db.restaurants.find( {"cuisine": "chicken", "address.zipcode": "10024"}


-- 10. Retrieve restaurants whose cuisine is chicken or whose zip code is 10024.
db.restaurants.find( {$or: [{"cuisine": "Chicken"}, {"address.zipcode": "10024"}]} ).pretty()


-- 11. Retrieve restaurants whose borough is Queens, cuisine is Jewish/kosher, sort by descending order of zipcode.
db.restaurants.find( {"borough": "Queens", "cuisine": "Jewish/Kosher"} ).sort( {"address.zipcode": -1} ).pretty()


-- 12. Retrieve restaurants with a grade A.
db.restaurants.find( {"grades.grade": "A"} ).pretty()


-- 13. Retrieve restaurants with a grade A, displaying only collection id, restaurant name, and grade.
db.restaurants.find( {"grades.grade": "A"}, {"name": 1, "grades.grade": 1} ).pretty()


-- 14. Retrieve restaurants with a grade A, displaying only restaurant name, and grade (no collection id):
db.restaurants.find( {"grades.grade": "A"}, {"name": 1, "grades.grade": 1, "_id": 0} ).pretty()


-- 15. Retrieve restaurants with a grade A, sort by cuisine ascending, and zip code descending.
db.restaurants.find( {"grades.grade": "A"} ).sort( {"cuisine": 1, "address.zipcode": -1} ).pretty()


-- 16. Retrieve restaurants with a score higher than 80.
db.restaurants.find( {"grades.score": {$gt: 80} } ).pretty()


/* 17. Insert a record with the following data:
street = 7th Avenue
zip code = 10024
building = 1000
coord = -58.9557413, 31.7720266 borough = Brooklyn
cuisine = BBQ
date = 2015-11-05T00:00:00Z 
grade : C
score = 15
name = Big Tex
restaurant_id = 61704627
*/
db.restaurants.insert(
    {
        "address": {
            "street": "7th Avenue",
            "zipcode": "10024",
            "building": "1000",
            "coord": [-58.9557413, 31.7720266]
        },
        "borough": "Brooklyn",
        "cuisine": "BBQ",
        "grades": [
            {
                "date": ISODate("2015-11-05T00:00:00Z"),
                "grade": "C",
                "score": 15
            }
        ],
        "name": "Big Tex",
        "restaurant_id": "61704627"
    }
);

-- Display document
db.restaurants.find( {"restaurant_id": "61704627"} ).pretty()


-- 18. Update the following record: Change the first White Castle restaurant document's cuisine to "Steak and Sea Food," and update the lastModified field with the current date.
-- 1. Find _id of document and display before change
db.restaurants.find( {"name": "White Castle"} ).limit(1).pretty()

-- 2. Update
db.restaurants.update(
    {"_id": ObjectId("62572fc7f3d3488647bdae3d")},
    {
        $set: {"cuisine": "Steak and Sea Food"}, $currentDate: {"lastModified": true}
    }
)

-- 3. Show updated document
db.restaurants.find( {"name": "White Castle"} ).limit(1).pretty()


-- 19. Delete the following records: Delete all White Castle restaurants.
-- 1. Display number of documents before delete
db.restaurants.find( {"name": "White Castle"} ).count()

-- 2. Delete documents
db.restaurants.remove( {"name": "White Castle"} )

-- 3. Display count
db.restaurants.find( {"name": "White Castle"} ).count()