# LIS3781 – Advanced Database Management

## Michael Proeber

#### Link to NoSQL query code

[MongoDB NoSQL code](../MongoDB_Queries.sql "NoSQL code")

#### Screenshots

##### 1. Display all documents in collection.

![Number 1](img/1.png)

##### 2a. Display the number of documents in collection.

![Number 2](img/2a.png)

##### 2b. Retrieve last document.

![Number 2](img/2b.png)

##### 3. Retrieve 1st 5 documents.

![Number 3](img/3.png)

##### 4a. Retrieve restaurants in the Brooklyn borough.

![Number 4](img/4a.png)

##### 4b. Count number of restaurants in the Brooklyn borough.

![Number 4](img/4b.png)

##### 4c. Case insensitive search using regex

![Number 4](img/4c.png)

##### 5a. Retrieve restaurants whose cuisine is American.

![Number 5](img/5.png)

##### 6a. Retrieve restaurants whose borough is Manhattan and cuisine is hamburgers.

![Number 6](img/6a.png)

##### 6b. Display only name and restaurant ID

![Number 6](img/6b.png)

##### 6c. Sort by name ascending, restaurant_id descending

![Number 6](img/6c.png)

##### 7. Display the number of restaurants whose borough is Manhattan and cuisine is hamburgers.

![Number 7](img/7.png)

##### 8. Query zip code field in embedded address document. Retrieve restaurants in the 10075 zip code area.

![Number 8](img/8.png)

##### 9. Retrieve restaurants whose cuisine is chicken and zipcode is 10024.

![Number 9](img/9.png)

##### 10. Retrieve restaurants whose cuisine is chicken or whose zip code is 10024.

![Number 10](img/10.png)

##### 11. Retrieve restaurants whose borough is Queens, cuisine is Jewish/kosher, sort by descending order of zipcode.

![Number 11](img/11.png)

##### 12. Retrieve restaurants with a grade A.

![Number 12](img/12.png)

##### 13. Retrieve restaurants with a grade A, displaying only collection id, restaurant name, and grade.

![Number 13](img/13.png)

##### 14. Retrieve restaurants with a grade A, displaying only restaurant name, and grade (no collection id):

![Number 14](img/14.png)

##### 15. Retrieve restaurants with a grade A, sort by cuisine ascending, and zip code descending.

![Number 15](img/15.png)

##### 16. Retrieve restaurants with a score higher than 80.

![Number 16](img/16.png)

##### 17a. Insert a record with the following data:

###### street = 7th Avenue

###### zip code = 10024

###### building = 1000

###### coord = -58.9557413, 31.7720266 borough = Brooklyn

###### cuisine = BBQ

###### date = 2015-11-05T00:00:00Z 

###### grade : C

###### score = 15

###### name = Big Tex

###### restaurant_id = 61704627

![Number 17](img/17a.png)

##### 17b. Display document

![Number 17](img/17b.png)

##### 18. Update the following record: Change the first White Castle restaurant document's cuisine to "Steak and Sea Food," and update the lastModified field with the current date.

###### 1. Find _id of document and display before change

![Number 18](img/18a.png)

###### 2. Update

![Number 18](img/18b.png)

###### 3. Show updated document

![Number 18](img/18c.png)

##### 19. Delete the following records: Delete all White Castle restaurants.

###### 1. Display number of documents before delete

![Number 19](img/19a.png)

###### 2. Delete documents

![Number 19](img/19b.png)

###### 3. Display count

![Number 19](img/19c.png)